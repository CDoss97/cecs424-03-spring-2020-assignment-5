%people
person(msbarrada).
person(msgort).
person(mrklatu).
person(mrnikto).

% ufo objects
ufo(balloon).
ufo(clothesline).
ufo(frisbee).
ufo(watertower).


solve :-
    %list of all ufo and people combos
    ufo(Barradaufo), ufo(Gortufo), ufo(Klatuufo), ufo(Niktoufo),
    unique([Barradaufo, Gortufo, Klatuufo, Niktoufo]),
	%list of all ufo and people for day
    person(Barrada), person(Gort), person(Klatu), person(Nikto),
    unique([Barrada, Gort, Klatu, Nikto]),

	%creates a set of placeholders for solving
    Sets = [ [Barrada, Barradaufo, tuesday],
                [Gort, Gortufo, wednesday],
                [Klatu, Klatuufo, thursday],
                [Nikto, Niktoufo, friday] ],

    %rule 1
	\+ member([ mrklatu , balloon, _],Sets),
	\+ member([ mrklatu , frisbee, _], Sets),
	\+ member([ msgort, frisbee , _], Sets),
    before([mrklatu, _, _], [_, balloon, _], Sets),
    before([_, frisbee, _], [mrklatu, _, _], Sets),

    %rule 2
    (member([msbarrada, _, friday], Sets);
	member([_, clothesline, friday], Sets) ;
    member([msbarrada, clothesline, friday], Sets)),

    %rule 3
    \+ member( [mrnikto, _ , tuesday ], Sets),

    %rule 4
	\+ member([ mrklatu , watertower , _ ], Sets),
    
	%applies rules
    tell(Barrada, Barradaufo, tuesday),
    tell(Gort, Gortufo, wednesday),
    tell(Klatu, Klatuufo, thursday),
    tell(Nikto, Niktoufo, friday).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
unique([X | T]) :- member(X, T), !, fail.        % (1)
unique([_ | T]) :- unique(T).                    % (2)
unique([_]).                                     % (3)

%applies timing rules 
before(X,_, [X |_ ]).
before(_, Y, [Y | _])  :- !, fail.
before(X, Y, [_ | T]) :- before(X,Y,T).
%outputs solution
tell(X, Y, Z) :-
    write(X), write(' saw the '), write(Y), write(' on '), write(Z), 
    write('.'), nl.